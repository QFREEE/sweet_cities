import 'jsdom-global/register';
import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import { assert, expect } from 'chai';
import Splash from './src/components/features/Splash';
import Adapter from 'enzyme-adapter-react-16';
import CityModel from './src/components/features/CityModel.js';
import CompanyModel from './src/components/features/CompanyModel';
import JobModel from './src/components/features/JobModel';
import City from './src/components/pagination/City';
import PaginationTemplate from './src/components/pagination/PaginationTemplate';
import NavigationBar from './src/components/features/NavigationBar';
import { Nav, Navbar } from "react-bootstrap";
import Search from './src/components/features/Search';
import Company from './src/components/pagination/Company';
import Job from './src/components/pagination/Job';

Enzyme.configure({ adapter: new Adapter() });

describe('<Splash />', () => {
  it('has an intro hero bg', function () {
    const wrapper = shallow(<Splash/>);
    expect(wrapper.find('#intro')).to.have.length(1);
  });

  it('has 3 model button links', function () {
    const wrapper = shallow(<Splash/>);
    expect(wrapper.find('Card')).to.have.length(3);
  });
});


describe('<CityModel />', () => {
	// basically checking that it rendered correctly.
	// I want to check if the componentDidMount()
	// actually got results from their API calls,
	// but I don't think this will work without a promise/callback,
	// and github says there's no good work around
	// TODO test for api setState() calls
	it('has valid initial state', function () {
		const wrapper = shallow(<CityModel/>);
		const instance = wrapper.instance();
		const state = instance.state;
		const initialState = {
			"cityIds":[],
			"loading":false,
			"currentPage":1,
			"numPostPerPage":9,
			"sortBy":"",
			"prevSortBy":"",
			"orderBy":"",
			"prevOrderBy":"",
			"filterBy":"",
			"prevFilterBy":"",
			"filterAttrBy":"",
			"prevFilterAttrBy":"",
			"filterValBy":"",
			"prevFilterValBy":"",
			"searchBy":"",
			"prevSearchBy":"",
			"searchByValue":"",
			"CompareInstances": []
		}
		assert.equal(JSON.stringify(state), JSON.stringify(initialState));
	});
});

describe('<CompanyModel />', () => {
	it('has valid initial state', function () {
		const wrapper = shallow(<CompanyModel/>);
		const instance = wrapper.instance();
		const state = instance.state;
		const initialState = {
			companyIds: [],
			loading: false,
			currentPage: 1,
			numPostPerPage: 9,
	  
			sortBy: "",
			prevSortBy: "",
	  
			orderBy: "",
			prevOrderBy: "",
	  
			filterBy: "",
			prevFilterBy: "",
			filterAttrBy: "",
			prevFilterAttrBy: "",
			filterValBy: "",
			prevFilterValBy: "",
	  
			searchBy: "",
			prevSearchBy: "",
			searchByValue: "",
			CompareInstances: []
		}
		assert.equal(JSON.stringify(state), JSON.stringify(initialState));
	});
});

describe('<JobModel />', () => {
	it('has valid initial state', function () {
		const wrapper = shallow(<JobModel />);
		const instance = wrapper.instance();
		const state = instance.state;
		const initialState = {
			jobIds: [],
			loading: false,
			currentPage: 1,
			numPostPerPage: 9,
	  
			sortBy: "",
			prevSortBy: "",
	  
			orderBy: "",
			prevOrderBy: "",
	  
			filterBy: "",
			prevFilterBy: "",
	  
			searchBy: "",
			prevSearchBy: "",
			searchByValue: "",
			CompareInstances: []
		}
		assert.equal(JSON.stringify(state), JSON.stringify(initialState));
	});
});

describe('<City />', () => {
	it('creates n number of Cards based on prop Instances', function () {
		const sampleData = [32, 5, 2, 5, 1];
		const wrapper = shallow(<City Instances={sampleData}/>);

		expect(wrapper.find('Card')).to.have.length(sampleData.length);
	});
});

describe('<Company />', () => {
	it('creates n number of Cards based on prop Instances', function () {
		const sampleData = [1, 2, 3, 4, 5, 6, 7, 8];
		const wrapper = shallow(<Company Instances={sampleData}/>);

		expect(wrapper.find('Card')).to.have.length(sampleData.length);
	});
});


describe('<Job />', () => {
	it('creates n number of Cards based on prop Instances', function () {
		const sampleData = [1, 2, 3, 4, 5, 6, 7, 8];
		const wrapper = shallow(<Job Instances={sampleData}/>);

		expect(wrapper.find('Card')).to.have.length(sampleData.length);
	});
});

describe('<PaginationTemplate />', () => {
	it('renders all page items', function () {
		const wrapper = shallow(<PaginationTemplate />);

		expect(wrapper.find('li')).to.have.length(4);
	});
});

describe('<NavigationBar />', () => {
	it('renders 6 nav links', function () {
		const wrapper = shallow(<NavigationBar />);
		expect(wrapper.find(Nav.Link)).to.have.length(5);
	});
});

describe('<Search />', () => {
	it('has valid initial state', function () {
		const wrapper = shallow(<Search />);
		const instance = wrapper.instance();
		const state = instance.state;
		const initialState = {
			currUrlBase: "",
			prevUrlBase: "",
			loading: true,
	  
			currentPage: 1,
			numPostPerPage: 9,
			tabIndex: 0,
			tabTitles: [],
	  
			cityIds: [],
			companyIds: [],
			jobIds: [],
			cityInfo: "",
			companyInfo: "",
			jobInfo: "",
		}
		assert.equal(JSON.stringify(state), JSON.stringify(initialState));
	})

	it('renders 0 cities with empty input', function () {
		const wrapper = shallow(<Search cityIds={[4, 2]}/>);
		expect(wrapper.find(City)).to.have.length(0);
	});
});