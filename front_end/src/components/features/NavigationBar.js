import React from "react";
import { Nav, Navbar } from "react-bootstrap";

class NavigationBar extends React.Component {
  render() {
    return (
      <nav>
        <Navbar collapseOnSelect expand="lg" bg="light" variant="light">
          <Navbar.Brand href="/">Sweet Cities</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="ml-auto">
              <Nav.Link href="/cities">Cities</Nav.Link>
              <Nav.Link href="/companies">Companies</Nav.Link>
              <Nav.Link href="/jobs">Job Roles</Nav.Link>
              <Nav.Link href="/visualizations">Visualizations</Nav.Link>
              <Nav.Link href="/About">About</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </nav>
    );
  }
}

export default NavigationBar;
