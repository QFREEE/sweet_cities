import React from "react";
import "../../css/Cards.css";
import { Redirect } from "react-router-dom";
import axios from "axios";
import Figure from "react-bootstrap/Figure";
import * as ReactBootStrap from "react-bootstrap";
import { Col, Row } from "react-bootstrap";
import Container from "react-bootstrap/Container";
import defaultImg from "../../images/default.png";
import Card from "react-bootstrap/Card";

class CompanyDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: "",
      name: "",
    };
    this.tryHandleImage = this.tryHandleImage.bind(this);
    this.fillTable = this.fillTable.bind(this);
    this.companyDescryption = this.companyDescryption.bind(this);
    this.getMap = this.getMap.bind(this);
  }

  async componentDidMount() {
    const temp = parseInt(this.props.match.params.companyName, 10);
    const url = "https://api.thesweetcities.com/companies/" + temp;
    axios
      .get(url)
      .then((response) => {
        this.setState({ data: response.data, name: response.data.name });
      })
      .catch( ()=> {
        Redirect("/404");
      });
  }

  getMap() {
    const baseUrl = "https://www.google.com/maps/embed/v1/place?zoom=10&key=";
    const googleApiKey = "AIzaSyAdFsiJxXkFo-858rSPZ0iGZ7lPtzbvBUg";
    const formatedAddress =
      this.state.data.company_address +
      ", " +
      this.state.data.company_city +
      ", " +
      this.state.data.company_state;
    const sourceUrl = baseUrl + googleApiKey + "&q=" + formatedAddress;
    console.log(formatedAddress);
    return (
      <iframe
        style={{
          marginLeft: "25%",
          marginTop: "2.5%",
          boxShadow: "5px 5px 30px -10px blue",
        }}
        width={550}
        height={400}
        src={sourceUrl}
      />
    );
  }

  fillTable() {
    var cityurl = "/cities";
    var cityN = "No City found for this Company";
    if (this.state.data) {
      if (this.state.data.cities_id != null) {
        if (this.state.data.cities_id[0])
          cityurl += "/" + this.state.data.cities_id[0];
          cityN = "Checkout a City for this Company!";
      }
    }

    var roleurl = "/jobs";
    var role = "No Jobs Found";
    if (this.state.data != null) {
      if (this.state.data.roles_id != null) {
        if (this.state.data.roles_id[0]) {
          roleurl += "/" + this.state.data.roles_id[0];
          role = "Checkout a Job in this City!";
        }
      }

      if (role == "No Jobs Found" && this.state.data.similar != null) {
        if (this.state.data.similar[0]) {
          roleurl += "/" + this.state.data.similar[0];
          role = "Checkout a Job in this City!";
        }
      }
    }

    var weburl = "/";
    var web = "No website Found";
    if (this.state.data != null) {
      if (this.state.data.company_weburl != "") {
        weburl = this.state.data.company_weburl;
        web = "Take me to this company's website!";
      }
    }

    return (
      <div>
        <tr>
          <td class="my_table_shadow">City</td>
          <td class="my_table_shadow">
            <Card.Link href={cityurl}>{cityN}</Card.Link>
          </td>
        </tr>

        <tr>
          <td class="my_table_shadow">State</td>
          <td class="my_table_shadow">{this.state.data.company_state}</td>
        </tr>

        <tr>
          <td class="my_table_shadow">Company's logo</td>
          <td class="my_table_shadow">{this.state.data.company_symbol}</td>
        </tr>

        <tr>
          <td class="my_table_shadow">Market Capitalization</td>
          <td class="my_table_shadow">
            {parseInt(this.state.data.company_marketCapitalization,10)}
          </td>
        </tr>

        <tr>
          <td class="my_table_shadow">Stock Price</td>
          <td class="my_table_shadow">{this.state.data.company_price_readable + " (" + parseInt(this.state.data.company_price, 10) + ")"}</td>
        </tr>

        <tr>
          <td class="my_table_shadow">Total Employees</td>
          <td class="my_table_shadow">
            {parseInt(this.state.data.company_employeeTotal, 10)}
          </td>
        </tr>

        <tr>
          <td class="my_table_shadow">roles</td>
          <td class="my_table_shadow">
            <Card.Link href={roleurl}>{role}</Card.Link>
          </td>
        </tr>

        <tr>
          <td class="my_table_shadow">Contact</td>
          <td class="my_table_shadow">{parseInt(this.state.data.company_phone, 10)}</td>
        </tr>

        <tr>
          <td class="my_table_shadow">Weburl</td>
          <td class="my_table_shadow">
            <Card.Link href={weburl}>{web}</Card.Link>
          </td>
        </tr>
      </div>
    );
  }

  // helper function to get image from unsplash.com
  tryHandleImage() {
    var instanceImgSrc = defaultImg;
    if (this.state.data.picture_url != "") {
      instanceImgSrc = this.state.data.picture_url;
    }

    return (
      <Figure.Image
        rounded
        width={450}
        height={350}
        src={instanceImgSrc}
        style={{
          marginLeft: "13%",
          marginTop: "5%",
          marginBottom: "13%",
          marginRight: "13%",
          boxShadow: "5px 5px 30px -10px blue",
        }}
      />
    );
  }

  companyDescryption() {
    if (this.state.data != null) {
      if (this.state.data.company_description != null) {
        return (
          <Card.Text class="my_table_shadow">
            {this.state.data.company_description}
          </Card.Text>
        );
      }
    }
    return (
      <header>
        <h1 class="text-dark my_text_shadow">
          No information found about the company
        </h1>
      </header>
    );
  }

  render() {
    return (
      <div>
        <Row>
          <Col style={{ paddingTop: "2%" }}>
            <header>
              <h1 class="text-dark my_text_shadow">{this.state.name}</h1>
            </header>
          </Col>
        </Row>

        <Container>
          <Row>
            <Col style={{ paddingTop: "2%" }}>
              <this.companyDescryption />
            </Col>
          </Row>

          <hr
            style={{
              color: "#000000",
              backgroundColor: "#000000",
            }}
          ></hr>

          <Row>
            <Col>
              <this.tryHandleImage
                company={this.props.match.params.companyName}
              />
            </Col>

            <Col md={4.5}>
              <ReactBootStrap.Table
                striped
                hover
                style={{
                  marginTop: "5%",
                  boxShadow: "5px 5px 30px -10px blue",
                }}
              >
                <tbody>
                  <this.fillTable />
                </tbody>
              </ReactBootStrap.Table>
              <Card.Text style={{ textAlign: "right" }}>
                Note: (ND) means no data available
              </Card.Text>
            </Col>
          </Row>

          <hr
            style={{
              color: "#000000",
              backgroundColor: "#000000",
            }}
          ></hr>

          <Row>
            <Col>
              <header>
                <h1 class="text-dark my_text_shadow" center>
                  Here's a map for you!
                </h1>
              </header>
            </Col>
          </Row>

          <Row>
            <Col class="center">
              <this.getMap />
              <Card.Text
                style={{
                  textAlign: "center",
                  marginTop: "1%",
                  marginBottom: "2.5%",
                }}
              >
                A map to show a Location of this company!
              </Card.Text>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default CompanyDetails;
