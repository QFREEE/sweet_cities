import React, { Component } from "react";
import "../../css/splash.css";
import city from "../../images/city-skyline.svg";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Typed from "react-typed";
import CityImg from "../../images/CityImg.jpg";
import Figure from "react-bootstrap/Figure";
import { Col, Row } from "react-bootstrap";
import { Redirect } from "react-router-dom";
import CompanyImg from "../../images/CompanyImg.jpg";
import JobImg from "../../images/JobImg.png";

class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: "",
      redirect: false,
    };
  }

  submitHandler = (event) => {
    event.preventDefault();
    this.setState({ redirect: true });
  };

  render() {
    if (this.state.redirect && this.state.inputValue != "") {
      return <Redirect to={{ pathname: "/search/"+this.state.inputValue }} />;
    }

    var isEnabled = this.state.inputValue.length > 0;
    return (
      <>
        <section id="intro" className="clearfix">
          <div className="container">
            <div className="box-left wobble">
              <Typed
                typedRef={(typed) => {
                  this.typed = typed;
                }}
                strings={["Home^3000", "Work^3000", "Life^3000"]}
                typeSpeed={150}
                loop={true}
                backSpeed={150}
              />
              <h1 className="my_text_shadow">Reimagined</h1>
              <p>
                Discover a new place to live and work as a software engineer
              </p>
              <form onSubmit={this.submitHandler}>
                <div
                  className="form-group d-inline-block align-middle"
                  style={{ marginRight: "10px" }}
                >
                  <input
                    type="text"
                    className="form-control"
                    id="inputCity"
                    aria-describedby="inputCity"
                    placeholder="Explore All Models!"
                    value={this.state.inputValue}
                    onChange={(e) => {
                      this.setState({
                        inputValue: e.target.value,
                      });
                    }}
                    style={{ boxShadow: "5px 5px 30px -10px black" }}
                  />
                </div>
                <Button
                  href={"/search/" + this.state.inputValue}
                  variant="primary"
                  type="submit"
                  className="d-inline-block"
                  style={{ boxShadow: "5px 5px 30px -10px black" }}
                  disabled={!isEnabled}
                >
                  Explore
                </Button>
              </form>
            </div>
            <div className="box-right">
              <img src={city} alt="city skyline" className="img-fluid" />
            </div>
          </div>
        </section>

        <section id="models">
          <div className="container">
            <div className="row text-center">
              <div className="col">
                <h2 className="my_text_shadow" style={{ margin: "50px 0" }}>
                  Check out our data and start discovering new places to live!
                </h2>
              </div>
            </div>
            <Row>
              <Col className="col-sm-4">
                <Card
                  className="bg-blue text-white text-center"
                  style={{
                    height: "15rem",
                    margin: 5,
                    boxShadow: "5px 5px 30px -10px black",
                  }}
                >
                  <Card.Body>
                    <Figure.Image
                      rounded
                      src={CityImg}
                      style={{
                        marginBottom: "8%",
                        marginRight: "13%",
                        boxShadow: "5px 5px 30px -10px black",
                        height: "145px",
                        display: "block",
                        marginLeft: "auto",
                        marginRight: "auto",
                      }}
                    />
                    <Button
                      href="/cities"
                      variant="primary"
                      style={{ boxShadow: "5px 5px 30px -10px black" }}
                    >
                      Explore our cities!
                    </Button>
                  </Card.Body>
                </Card>
              </Col>
              <Col className="col-sm-4">
                <Card
                  className="bg-blue text-white text-center"
                  style={{
                    height: "15rem",
                    margin: 5,
                    boxShadow: "5px 5px 30px -10px black",
                  }}
                >
                  <Card.Body>
                    <Figure.Image
                      rounded
                      src={CompanyImg}
                      style={{
                        marginBottom: "8%",
                        boxShadow: "5px 5px 30px -10px black",
                        height: "145px",
                      }}
                    />
                    <Button
                      href="/companies"
                      variant="primary"
                      style={{ boxShadow: "5px 5px 30px -10px black" }}
                    >
                      Explore our companies!
                    </Button>
                  </Card.Body>
                </Card>
              </Col>
              <Col className="col-sm-4">
                <Card
                  className="bg-blue text-white text-center"
                  style={{
                    height: "15rem",
                    margin: 5,
                    boxShadow: "5px 5px 30px -10px black",
                  }}
                >
                  <Card.Body>
                    <Figure.Image
                      rounded
                      src={JobImg}
                      style={{
                        marginBottom: "8%",
                        boxShadow: "5px 5px 30px -10px black",
                        height: "145px",
                        display: "block",
                        marginLeft: "auto",
                        marginRight: "auto",
                      }}
                    />
                    <Button
                      style={{ boxShadow: "5px 5px 30px -10px black" }}
                      href="/jobs"
                      variant="primary"
                    >
                      Explore our jobs!
                    </Button>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
            <br />
            <br />
            <br />
          </div>
        </section>
      </>
    );
  }
}

export default Splash;
