import React from "react";
import "../../css/Cards.css";
import axios from "axios";
import Figure from "react-bootstrap/Figure";
import { Redirect } from "react-router-dom";
import * as ReactBootStrap from "react-bootstrap";
import defaultImg from "../../images/default.png";
import { Col, Row } from "react-bootstrap";
import Container from "react-bootstrap/Container";
import Card from "react-bootstrap/Card";
import ReactHtmlParser from "react-html-parser";
import ReactPlayer from "react-player";

class JobDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: "",
      name: "",
    };
    this.tryHandleImage = this.tryHandleImage.bind(this);
    this.fillTable = this.fillTable.bind(this);
    this.roleDescryption = this.roleDescryption.bind(this);
    this.dailyResponsibility = this.dailyResponsibility.bind(this);
    this.skills = this.skills.bind(this);
    this.addVideo = this.addVideo.bind(this);
  }

  async componentDidMount() {
    const temp = parseInt(this.props.match.params.jobName, 10);
    const url = "https://api.thesweetcities.com/roles/" + temp;
    axios
      .get(url)
      .then((response) => {
        this.setState({ data: response.data, name: response.data.name });
      })
      .catch(() => {
        Redirect("/404");
      });
  }

  roleDescryption() {
    if (this.state.data != null) {
      if (this.state.data.role_description != null) {
        return (
          <div>
            <Card.Text class="my_table_shadow">
              Job Descryption: <br />
            </Card.Text>
            <Card.Text class="my_table_shadow">
              {ReactHtmlParser(this.state.data.role_description)}
            </Card.Text>
          </div>
        );
      }
    }

    return (
      <header>
        <h1 class="text-dark my_text_shadow">
          No information found about the role
        </h1>
      </header>
    );
  }

  addVideo() {
    var title = "Opps, something went wrong! -- Sorry";
    var embededUrl = "https://www.youtube.com/embed/GY8PkikQ8ZE";
    if (this.state.data.video_url && this.state.data.video_url != "") {
      embededUrl = this.state.data.picture_url;
      embededUrl = "Some insights about the Job's Company!";
    }

    return (
      <div>
        <ReactPlayer
          style={{
            marginLeft: "20%",
            marginTop: "2.5%",
            boxShadow: "5px 5px 30px -10px blue",
          }}
          url={embededUrl}
        />
        <Card.Text
          style={{ textAlign: "center", marginTop: "1%", marginBottom: "1%" }}
        >
          {title}
        </Card.Text>
      </div>
    );
  }

  fillTable() {
    var cityurl = "/cities";
    var cityN = "No City found for this Role";
    if (this.state.data != null) {
      if (this.state.data.cities_id != null) {
        if (this.state.data.cities_id[0])
          cityurl += "/" + this.state.data.cities_id[0];
          cityN = "Checkout a City for this Role!";
      }
    }

    var companyurl = "/companies";
    var company = "No Company Found";
    if (this.state.data != null) {
      if (this.state.data.companies_id != null) {
        if (this.state.data.companies_id[0]) {
          companyurl += "/" + this.state.data.companies_id[0];
          company = "Check it out!";
        }
      }

      if (company == "No Company Found" && this.state.data.similar != null) {
        if (this.state.data.similar[1]) {
          companyurl += "/" + this.state.data.similar[1];
          company = "Check it out!";
        }
      }
    }

    return (
      <div>
        <tr>
          <td class="my_table_shadow">State</td>
          <td class="my_table_shadow">{this.state.data.role_state}</td>
        </tr>

        <tr>
          <td class="my_table_shadow">State</td>
          <td class="my_table_shadow">
            {parseInt(this.state.data.median_salary, 10)}
          </td>
        </tr>

        <tr>
          <td class="my_table_shadow">High Salary</td>
          <td class="my_table_shadow">
            {parseInt(this.state.data.high_salary, 10)}
          </td>
        </tr>

        <tr>
          <td class="my_table_shadow">Low Salary</td>
          <td class="my_table_shadow">
            {parseInt(this.state.data.low_salary, 10)}
          </td>
        </tr>

        <tr>
          <td class="my_table_shadow">City</td>
          <td class="my_table_shadow">
            <Card.Link href={cityurl}>{cityN}</Card.Link>
          </td>
        </tr>

        <tr>
          <td class="my_table_shadow">Company</td>
          <td class="my_table_shadow">
            <Card.Link href={companyurl}>{company}</Card.Link>
          </td>
        </tr>
      </div>
    );
  }

  // helper function to get image from unsplash.com
  tryHandleImage = (instanceObject) => {
    var instanceImgSrc = defaultImg;
    if (
      this.state.data.picture_url &&
      this.state.data.picture_url != ""
    ) {
      instanceImgSrc = this.state.data.picture_url;
    }

    return (
      <Figure.Image
        rounded
        width={450}
        height={350}
        src={instanceImgSrc}
        style={{
          marginLeft: "13%",
          marginTop: "5%",
          marginBottom: "13%",
          marginRight: "13%",
          boxShadow: "5px 5px 30px -10px blue",
        }}
      />
    );
  };

  dailyResponsibility() {
    if (this.state.data != null) {
      if (this.state.data.daily_responsibility != null) {
        return (
          <div>
            <Card.Text class="my_table_shadow">
              Daily Responsibilities: <br />
            </Card.Text>
            <Card.Text class="my_table_shadow">
              {ReactHtmlParser(this.state.data.daily_responsibility)}
            </Card.Text>
          </div>
        );
      }
    }

    return (
      <header>
        <h1 class="text-dark my_text_shadow">
          No information found about the role
        </h1>
      </header>
    );
  }

  skills() {
    if (this.state.data != null) {
      if (this.state.data.skills != null) {
        return (
          <div>
            <Card.Text class="my_table_shadow">
              Preffered Skills: <br />
            </Card.Text>
            <Card.Text class="my_table_shadow">
              {ReactHtmlParser(this.state.data.skills)}
            </Card.Text>
          </div>
        );
      }
    }

    return (
      <header>
        <h1 class="text-dark my_text_shadow">
          No information found about the role
        </h1>
      </header>
    );
  }

  render() {
    return (
      <div>
        <Row>
          <Col style={{ paddingTop: "2%" }}>
            <header>
              <h1 class="text-dark my_text_shadow">{this.state.name}</h1>
            </header>
          </Col>
        </Row>

        <Container>
          <this.roleDescryption />

          <hr
            style={{
              color: "#000000",
              backgroundColor: "#000000",
            }}
          ></hr>

          <Row>
            <Col>
              <this.tryHandleImage job={this.props.match.params.jobName} />
            </Col>

            <Col md={4.5}>
              <ReactBootStrap.Table
                striped
                hover
                style={{
                  marginTop: "5%",
                  boxShadow: "5px 5px 30px -10px blue",
                }}
              >
                <tbody>
                  <this.fillTable />
                </tbody>
              </ReactBootStrap.Table>
              <Card.Text style={{ textAlign: "right" }}>
                Note: (ND) means no data available
              </Card.Text>
            </Col>
          </Row>

          <hr
            style={{
              color: "#000000",
              backgroundColor: "#000000",
            }}
          ></hr>

          <Row>
            <Col>
              <this.addVideo />
            </Col>
          </Row>

          <hr
            style={{
              color: "#000000",
              backgroundColor: "#000000",
            }}
          ></hr>

          <Row>
            <Col style={{ paddingTop: "2%" }}>
              <this.dailyResponsibility />

              <this.skills />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default JobDetails;
