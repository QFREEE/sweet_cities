import React from "react";
import { Col, Row } from "react-bootstrap";
import { useState, useEffect } from "react";
import axios from "axios";
import Container from "react-bootstrap/Container";
import "../../css/Cards.css";
import Figure from "react-bootstrap/Figure";
import { Redirect } from "react-router-dom";
import defaultImg from "../../images/default.png";
import * as ReactBootStrap from "react-bootstrap";
import Card from "react-bootstrap/Card";
import ReactPlayer from "react-player";

class CityDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: "",
      name: "",
    };
    this.tryHandleImage = this.tryHandleImage.bind(this);
    this.fillTable = this.fillTable.bind(this);
    this.addVideo = this.addVideo.bind(this);
  }

  async componentDidMount() {
    const temp = parseInt(this.props.match.params.cityName, 10);
    const url = "https://api.thesweetcities.com/cities/" + temp;
    axios
      .get(url)
      .then((response) => {
        console.log(response.data);
        this.setState({ data: response.data, name: response.data.name });
      })
      .catch(() => {
        Redirect("/404");
      });
  }

  addVideo() {
    var videoUrl = "https://www.youtube.com/embed/GY8PkikQ8ZE";
    var title = "Opps, something went wrong! -- Sorry";

    if (
      this.state.data.video_url &&
      this.state.data.video_url != "https://www.youtube.com/embed/GY8PkikQ8ZE"
    ) {
      videoUrl = this.state.data.video_url;
      title = "Something fun about this city!";
    }

    return (
      <div>
        <ReactPlayer
          style={{
            marginLeft: "20%",
            marginTop: "2.5%",
            boxShadow: "5px 5px 30px -10px blue",
          }}
          url={videoUrl}
        />
        <Card.Text
          style={{ textAlign: "center", marginTop: "1%", marginBottom: "1%" }}
        >
          {title}
        </Card.Text>
      </div>
    );
  }

  fillTable() {
    var companyurl = "/companies";
    var company = "No Company Found";
    if (this.state.data != null) {
      if (this.state.data.companies_id != null) {
        if (this.state.data.companies_id[0]) {
          companyurl += "/" + this.state.data.companies_id[0];
          company = "Check it out!";
        }
      }

      if (company == "No Company Found" && this.state.data.similar != null) {
        if (this.state.data.similar[1]) {
          companyurl += "/" + this.state.data.similar[1];
          company = "Check it out!";
        }
      }
    }

    var city_weather = "Weather not available";
    if (this.state.data !== null) {
      if (this.state.data.city_weather != null) {
        city_weather = this.state.data.city_weather;
        console.log(city_weather);
        var weather = city_weather.split(", ");
        city_weather = "" + weather[0] + "℉" + ", " + weather[1];
      }
    }

    var roleurl = "/jobs";
    var role = "No Jobs Found";
    if (this.state.data != null) {
      if (this.state.data.roles_id != null) {
        if (this.state.data.roles_id[0]) {
          roleurl += "/" + this.state.data.roles_id[0];
          role = "Checkout a Job in this City!";
        }
      }

      if (role == "No Jobs Found" && this.state.data.similar != null) {
        if (this.state.data.similar[0]) {
          roleurl += "/" + this.state.data.similar[0];
          role = "Checkout a Job in this City!";
        }
      }
    }

    return (
      <div>
        <tr>
          <td class="my_table_shadow">State</td>
          <td class="my_value_shadow">{this.state.data.city_state}</td>
        </tr>

        <tr>
          <td class="my_table_shadow">Recent Weather</td>
          <td class="my_value_shadow">{city_weather}</td>
        </tr>

        <tr>
          <td class="my_table_shadow">Climate Index</td>
          <td class="my_value_shadow">{this.state.data.climate_index_readable + " (" + parseInt(this.state.data.climate_index, 10) + ")"}</td>
        </tr>

        <tr>
          <td class="my_table_shadow">Pollution Index</td>
          <td class="my_value_shadow">{this.state.data.pollution_readable + " (" + parseInt(this.state.data.pollution_index, 10) + ")"}</td>
        </tr>

        <tr>
          <td class="my_table_shadow">Cost Of Living Index</td>
          <td class="my_value_shadow">
            {this.state.data.restaurant_price_index_readable + " (" + parseInt(this.state.data.restaurant_price_index, 10) + ")"}
          </td>
        </tr>

        <tr>
          <td class="my_table_shadow">Health Care Index</td>
          <td class="my_value_shadow">{this.state.data.health_care_readable + " (" + parseInt(this.state.data.health_care_index, 10) + ")"}</td>
        </tr>

        <tr>
          <td class="my_table_shadow">Company in this city</td>
          <td class="my_table_shadow">
            <Card.Link href={companyurl}>{company}</Card.Link>
          </td>
        </tr>

        <tr>
          <td class="my_table_shadow">Possible job</td>
          <td class="my_table_shadow">
            <Card.Link href={roleurl}>{role}</Card.Link>
          </td>
        </tr>
      </div>
    );
  }

  // helper function to get image from unsplash.com
  tryHandleImage(instanceObject) {
    var instanceImgSrc = defaultImg;

    if (
      this.state.data.picture_url != null &&
      this.state.data.picture_url != ""
    ) {
      instanceImgSrc = this.state.data.picture_url;
    }

    return (
      <Figure.Image
        rounded
        width={450}
        height={350}
        src={instanceImgSrc}
        style={{
          marginLeft: "13%",
          marginTop: "5%",
          marginBottom: "13%",
          marginRight: "13%",
          boxShadow: "5px 5px 30px -10px blue",
        }}
      />
    );
  }

  render() {
    return (
      <div>
        <Row>
          <Col style={{ paddingTop: "2%" }}>
            <header>
              <h1 class="text-dark my_text_shadow">{this.state.name}</h1>
            </header>
          </Col>
        </Row>

        <Container>
          <Row>
            <Col>
              <this.addVideo />
            </Col>
          </Row>

          <hr
            style={{
              color: "#000000",
              backgroundColor: "#000000",
            }}
          ></hr>

          <Row>
            <Col>
              <this.tryHandleImage city={this.props.match.params.cityName} />
            </Col>

            <Col md={4.5}>
              <ReactBootStrap.Table
                striped
                hover
                style={{
                  marginTop: "5%",
                  boxShadow: "5px 5px 30px -10px blue",
                }}
              >
                <tbody>
                  <this.fillTable />
                </tbody>
              </ReactBootStrap.Table>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default CityDetails;
