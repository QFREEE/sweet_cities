import React, { Component } from "react";
import Container from "react-bootstrap/Container";
import { Row, Col } from "react-bootstrap";
import Card from "react-bootstrap/Card";
import { CardDeck } from "react-bootstrap";
import defaultImg from "../../images/default.png";

import Highlight from "react-highlighter";

class City extends Component {
  constructor(props) {
    super(props);
  }

  fillCardTitle = (instanceObject) => {
    const cardTitle = instanceObject.instance.name;

    if (cardTitle == null || cardTitle == "") {
      return <p>Card title does not exist</p>;
    }
    return <Highlight search={this.props.search}>{cardTitle}</Highlight>;
  };

  fancyDisplay = (temp) => {
    var result = "";
    if (temp > 80.0) {
      result = "High (" + temp + ")";
    } else if (temp < 80.0 && temp > 50.0) {
      result = "Medium (" + temp + ")";
    } else {
      result = "Low (" + temp + ")";
    }
    return <Highlight search={this.props.search}>{result}</Highlight>;
  };

  fillCard = (instanceObject) => {
    const cardDetails = (
      <Card.Text>
        <Highlight search={this.props.search}>State: </Highlight>
        <Highlight search={this.props.search}>
          {instanceObject.instance.city_state}
        </Highlight>
        <br />
        <Highlight search={this.props.search}>Cost Of Living: </Highlight>
        <Highlight search={this.props.search}>
          {instanceObject.instance.restaurant_price_index_readable +
            " (" +
            parseInt(instanceObject.instance.restaurant_price_index, 10) +
            ")"}
          <br />
        </Highlight>
        <Highlight search={this.props.search}>Pollution Index: </Highlight>
        <Highlight search={this.props.search}>
          {instanceObject.instance.pollution_readable +
            " (" +
            parseInt(instanceObject.instance.pollution_index, 10) +
            ")"}
        </Highlight>
        <br />
        <Highlight search={this.props.search}>Health Care Index: </Highlight>
        <Highlight search={this.props.search}>
          {instanceObject.instance.health_care_readable +
            " (" +
            parseInt(instanceObject.instance.health_care_index, 10) +
            ")"}
        </Highlight>
        <br />
        <Highlight search={this.props.search}>Climate Index: </Highlight>
        <Highlight search={this.props.search}>
          {instanceObject.instance.climate_index_readable +
            " (" +
            parseInt(instanceObject.instance.climate_index, 10) +
            ")"}
        </Highlight>
        <br />
      </Card.Text>
    );

    if (cardDetails == null || cardDetails == "") {
      return <p>card data does not exist</p>;
    }
    return cardDetails;
  };

  tryHandleImage = (instanceObject) => {
    var instanceImgSrc = defaultImg;
    if (
      instanceObject.instance != null &&
      instanceObject.instance.picture_url &&
      instanceObject.instance.picture_url != ""
    ) {
      instanceImgSrc = instanceObject.instance.picture_url;
    }

    return (
      <Card.Img
        width={350}
        height={250}
        src={instanceImgSrc}
        style={{ boxShadow: "1px 1px 15px -5px white" }}
      />
    );
  };

  fillFooter = (instanceObject) => {
    if (this.props.fromSearch) {
      // do not
    }

    return (
      <div>
        <Row>
          <Col>
            <Card.Link
              href={instanceObject.url + instanceObject.instance.id}
              variant="primary"
              style={{ bottom: 0 }}
            >
              Learn more
            </Card.Link>
          </Col>

          {this.addCompare(instanceObject)}
        </Row>
      </div>
    );
  };

  addCompare = (instanceObject) => {
    if (!this.props.fromSearch) {
      return (
        <Col>
          <Card.Text
            onClick={() => {
              console.log(instanceObject.instance);
              this.props.populateCompareCityIds(instanceObject.instance);
            }}
          >
            Compare
          </Card.Text>
        </Col>
      );
    }
    return <></>;
  };

  render() {
    if (this.props.loading) {
      return <h2 class="text-dark my_text_shadow text-center">Loading ...</h2>;
    }

    const url = "/cities/";
    return (
      <Container>
        <CardDeck>
          {this.props.Instances.map((instance) => (
            <Col>
              <Card
                key={instance.intance}
                className="bg-dark text-white"
                style={{
                  width: "18rem",
                  height: "32rem",
                  margin: 5,
                  boxShadow: "5px 5px 30px -10px #1743a5",
                }}
              >
                <Card.Body>
                  <this.fillCardTitle instance={instance} />

                  <this.tryHandleImage instance={instance} />

                  <this.fillCard instance={instance} />
                </Card.Body>

                <Card.Footer>
                  <this.fillFooter instance={instance} url={url} />
                </Card.Footer>
              </Card>
            </Col>
          ))}
        </CardDeck>
      </Container>
    );
  }
}

export default City;
