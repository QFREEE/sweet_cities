import React from "react";
import Container from "react-bootstrap/Container";
import Card from "react-bootstrap/Card";
import { CardDeck } from "react-bootstrap";
import { useState, useEffect } from "react";
import axios from "axios";
import defaultImg from "../../images/default.png";
import { Row, Col } from "react-bootstrap";

import Highlight from "react-highlighter";

class Job extends React.Component {
  constructor(props) {
    super(props);
  }

  fillCardTitle = (instanceObject) => {
    const cardTitle = instanceObject.instance.name;

    if (cardTitle == null || cardTitle == "") {
      return <p>Card title does not exist</p>;
    }
    return <Highlight search={this.props.search}>{cardTitle}</Highlight>;
  };

  tryHandleImage = (instanceObject) => {
    var instanceImgSrc = defaultImg;
    if (
      instanceObject.instance != null &&
      instanceObject.instance.picture_url &&
      instanceObject.instance.picture_url != ""
    ) {
      instanceImgSrc = instanceObject.instance.picture_url;
    }

    return (
      <Card.Img
        width={350}
        height={250}
        src={instanceImgSrc}
        style={{ boxShadow: "1px 1px 15px -5px white" }}
      />
    );
  };

  fillCard = (instanceObject) => {
    const cardDetails = (
      <Card.Text>
        <Highlight search={this.props.search}>State:</Highlight>{" "}
        <Highlight search={this.props.search}>
          {instanceObject.instance.role_state}
        </Highlight>
        <br></br>
        <Highlight search={this.props.search}>Median Salaray: </Highlight>{" "}
        <Highlight search={this.props.search}>
          {parseInt(instanceObject.instance.median_salary, 10)}
        </Highlight>
        <br></br>
        <Highlight search={this.props.search}>High Salary: </Highlight>{" "}
        <Highlight search={this.props.search}>
          {parseInt(instanceObject.instance.high_salary, 10)}
        </Highlight>
        <br></br>
        <Highlight search={this.props.search}>Low Salary: </Highlight>{" "}
        <Highlight search={this.props.search}>
          {parseInt(instanceObject.instance.low_salary, 10)}
        </Highlight>
        <br></br>
      </Card.Text>
    );

    if (cardDetails == null || cardDetails == "") {
      return <p>card data does not exist</p>;
    }
    return cardDetails;
  };

  fillFooter = (instanceObject) => {
    return (
      <div>
        <Row>
          <Col>
            <Card.Link
              href={instanceObject.url + instanceObject.instance.id}
              variant="primary"
              style={{ bottom: 0 }}
            >
              Learn more
            </Card.Link>
          </Col>

          {this.addCompare(instanceObject)}
        </Row>
      </div>
    );
  };

  addCompare = (instanceObject) => {
    if (!this.props.fromSearch) {
      return (
        <Col>
          <Card.Text
            onClick={() => {
              console.log(instanceObject.instance);
              this.props.populateCompareInstances(instanceObject.instance);
            }}
          >
            Compare
          </Card.Text>
        </Col>
      );
    }
    return <></>;
  };

  render() {
    if (this.props.loading) {
      return <h2 class="text-dark my_text_shadow text-center">Loading ...</h2>;
    }

    const url = "/jobs/";
    return (
      <Container>
        <CardDeck>
          {this.props.Instances.map((instance) => (
            <Col>
              <Card
                key={instance.intance}
                className="bg-dark text-white"
                style={{
                  width: "18rem",
                  height: "31rem",
                  margin: 5,
                  boxShadow: "5px 5px 30px -10px #1743a5",
                }}
              >
                <Card.Body>
                  <this.fillCardTitle instance={instance} />

                  <this.tryHandleImage instance={instance} />

                  <this.fillCard instance={instance} />
                </Card.Body>

                <Card.Footer>
                  <this.fillFooter instance={instance} url={url} />
                </Card.Footer>
              </Card>
            </Col>
          ))}
        </CardDeck>
      </Container>
    );
  }
}

export default Job;
