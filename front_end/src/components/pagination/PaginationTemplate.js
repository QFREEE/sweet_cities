import React from "react";

class PaginationTemplate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      numItems: 5,
    };
  }

  render() {
    const totalPages = Math.ceil(
      this.props.totalInstances / this.props.instancesPerPage
    );
    let sliceStart =
      Math.floor(this.props.currentPage / this.state.numItems) *
        this.state.numItems +
      1;
    if (this.props.currentPage % this.state.numItems == 0) {
      sliceStart -= this.state.numItems;
    }
    let sliceEnd =
      Math.floor(this.props.currentPage / this.state.numItems) *
        this.state.numItems +
      this.state.numItems;
    if (this.props.currentPage % this.state.numItems == 0) {
      sliceEnd -= this.state.numItems;
    }
    if (sliceEnd > totalPages) {
      sliceEnd = totalPages;
    }
    const pageNumbers = [];
    for (let i = sliceStart; i <= sliceEnd; i++) {
      pageNumbers.push(i);
    }

    const paginationCode = pageNumbers.map((number) => {
      if (number == this.props.currentPage) {
        return (
          <li className="page-item active">
            <a
              onClick={() =>
                this.props.paginate(number, this.props.totalInstances)
              }
              href="#"
              className="page-link"
            >
              <strong>{number}</strong>
            </a>
          </li>
        );
      } else {
        return (
          <li className="page-item">
            <a
              onClick={() =>
                this.props.paginate(number, this.props.totalInstances)
              }
              href="#"
              className="page-link"
            >
              {number}
            </a>
          </li>
        );
      }
    });

    if (totalPages == 0) {
      return <br />;
    }

    return (
      <div>
        <br />
        <br />
        <nav aria-lable="Pagination For Models">
          <ul className="pagination pagination-circle">
            <li className="page-item page-fpnl">
              <a
                onClick={() =>
                  this.props.paginate(1, this.props.totalInstances)
                }
                className="page-link"
                aria-label="First"
              >
                <span aria-hidden="true">&laquo;</span>
                <span className="sr-only">First</span>
              </a>
            </li>
            <li className="page-item page-fpnl">
              <a
                onClick={() =>
                  this.props.paginate(
                    this.props.currentPage - 1,
                    this.props.totalInstances
                  )
                }
                className="page-link"
                aria-label="Previous"
              >
                <span aria-hidden="true">&lsaquo;</span>
                <span className="sr-only">Previous</span>
              </a>
            </li>
            {paginationCode}
            <li className="page-item page-fpnl">
              <a
                onClick={() =>
                  this.props.paginate(
                    this.props.currentPage + 1,
                    this.props.totalInstances
                  )
                }
                className="page-link"
                aria-label="Next"
              >
                <span aria-hidden="true">&rsaquo;</span>
                <span className="sr-only">Next</span>
              </a>
            </li>
            <li className="page-item page-fpnl">
              <a
                onClick={() =>
                  this.props.paginate(totalPages, this.props.totalInstances)
                }
                className="page-link"
                aria-label="Last"
              >
                <span aria-hidden="true">&raquo;</span>
                <span className="sr-only">Last</span>
              </a>
            </li>
          </ul>
          <p className="total-pages">{totalPages} pages</p>
        </nav>
        <br />
      </div>
    );
  }
}

export default PaginationTemplate;
