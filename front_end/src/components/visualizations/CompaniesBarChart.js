import React from "react";
import * as d3 from "d3";
import axios from "axios";
import { Redirect } from "react-router-dom";

import "../../css/CompaniesBarChart.css";

class CompaniesBarChart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      companiesInfo: null,
    };
  }

  async componentDidMount() {
    const url = "https://api.thesweetcities.com/companies";

    axios
      .get(url)
      .then((response) => {
        //update this.state -- cityIds
        this.setState({ companiesInfo: response.data });
      })
      .catch(() => {
        Redirect("/404");
      });
  }

  componentDidUpdate() {
    const margin = 60;
    const width = 1300 - 2 * margin;
    const height = 600 - 2 * margin;

    const svg = d3.select("svg");

    const chart = svg
      .append("g")
      .attr("transform", `translate(${margin}, ${margin + 30})`);

    const yScale = d3.scaleLinear().range([height, 0]).domain([0, 35]);

    chart.append("g").call(d3.axisLeft(yScale));

    // Define the div for the tooltip
    var div = d3
      .select("body")
      .append("div")
      .attr("class", "tooltip")
      .text("")
      .style("fill", "#69b3a2");

    const states = [
      "AL",
      "AK",
      "AS",
      "AZ",
      "AR",
      "CA",
      "CO",
      "CT",
      "DE",
      "DC",
      "FM",
      "FL",
      "GA",
      "GU",
      "HI",
      "ID",
      "IL",
      "IN",
      "IA",
      "KS",
      "KY",
      "LA",
      "ME",
      "MH",
      "MD",
      "MA",
      "MI",
      "MN",
      "MS",
      "MO",
      "MT",
      "NE",
      "NV",
      "NH",
      "NJ",
      "NM",
      "NY",
      "NC",
      "ND",
      "MP",
      "OH",
      "OK",
      "OR",
      "PW",
      "PA",
      "PR",
      "RI",
      "SC",
      "SD",
      "TN",
      "TX",
      "UT",
      "VT",
      "VI",
      "VA",
      "WA",
      "WV",
      "WI",
      "WY",
    ];

    const state = [
      "Alabama",
      "Alaska",
      "American Samoa",
      "Arizona",
      "Arkansas",
      "California",
      "Colorado",
      "Connecticut",
      "Delaware",
      "District of Columbia",
      "Federated States of Micronesia",
      "Florida",
      "Georgia",
      "Guam",
      "Hawaii",
      "Idaho",
      "Illinois",
      "Indiana",
      "Iowa",
      "Kansas",
      "Kentucky",
      "Louisiana",
      "Maine",
      "Marshall Islands",
      "Maryland",
      "Massachusetts",
      "Michigan",
      "Minnesota",
      "Mississippi",
      "Missouri",
      "Montana",
      "Nebraska",
      "Nevada",
      "New Hampshire",
      "New Jersey",
      "New Mexico",
      "New York",
      "North Carolina",
      "North Dakota",
      "Northern Mariana Islands",
      "Ohio",
      "Oklahoma",
      "Oregon",
      "Palau",
      "Pennsylvania",
      "Puerto Rico",
      "Rhode Island",
      "South Carolina",
      "South Dakota",
      "Tennessee",
      "Texas",
      "Utah",
      "Vermont",
      "Virgin Island",
      "Virginia",
      "Washington",
      "West Virginia",
      "Wisconsin",
      "Wyoming",
    ];

    const xScale = d3.scaleBand().range([0, width]).domain(states).padding(0.2);

    chart
      .append("g")
      .attr("transform", `translate(0, ${height})`)
      .call(d3.axisBottom(xScale));

    var data = [];
    for (let s in states) {
      var st = states[s];
      data.push({ state: st, value: 0, fullName: state[s] });
    }

    for (let key in this.state.companiesInfo) {
      var company = this.state.companiesInfo[key];
      var company_state = company["company_state"];
      var index_state = state.indexOf(company_state);
      var abbr_state = states[index_state];
      for (let entry in data) {
        var st = data[entry];
        if (st.state == abbr_state) {
          st.value += 1;
        }
      }
    }

    // console.log(data);
    svg
      .selectAll("bar")
      .data(data)
      .enter()
      .append("rect")
      .attr("fill", function (d) {
        // return "rgb(0, 48, " + (255 - d.value * 10) + ")";
        return (
          "rgb(0," + (123 - d.value * 3) + ", " + (255 - d.value * 4) + ")"
        );
      })
      .attr("x", function (s) {
        return xScale(s.state) + 60;
      })
      .attr("width", xScale.bandwidth())
      .attr("y", function (s) {
        return yScale(s.value - 6.5);
      })
      .attr("height", function (s) {
        return height - yScale(s.value);
      })
      .on("mouseover", function (d) {
        div.transition().duration(200).style("opacity", 0.9);
        d3.select(this).attr("fill", "orange");
        var duration = 300;
        data.forEach(function (d, i) {
          // console.log(d.value);
          svg
            .transition()
            .duration(duration)
            .delay(i * duration)
            .attr("r", d.value);
        });
        div
          .html(d.fullName + ": <br>" + d.value)
          .style("left", d3.event.pageX + "px")
          .style("top", d3.event.pageY - 28 + "px");
      })
      .on("mouseout", function (d) {
        d3.select(this)
          .transition()
          .duration(250)
          .attr(
            "fill",
            "rgb(0," + (123 - d.value * 3) + ", " + (255 - d.value * 4) + ")"
          );
        div.transition().duration(200).style("opacity", 0);
      });

    chart
      .append("g")
      .attr("class", "grid")
      .call(d3.axisLeft().scale(yScale).tickSize(-width, 0, 0).tickFormat(""));

    svg
      .append("text")
      .attr("x", -(height / 2) - margin)
      .attr("y", margin / 2.4)
      .attr("transform", "rotate(-90)")
      .attr("text-anchor", "middle")
      .text("# Companies");

    svg
      .append("text")

      .attr("x", width / 2 + margin)
      .attr("y", 50)
      .attr("text-anchor", "middle")
      .style("font-size", "28px")
      .text(
        "Displays the number of companies in a state as reflected from our data"
      );
  }

  render() {
    return <svg width={1300} height={650}></svg>;
  }
}

export default CompaniesBarChart;
