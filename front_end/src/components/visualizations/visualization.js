import React from "react";

import { Tab, Tabs, TabList } from "react-tabs";

import JobsBubbleChart from "./JobsBubbleChart";
import CompaniesBarChart from "./CompaniesBarChart";
import CitiesMap from "./CitiesMap";
import Container from "react-bootstrap/Container";
import GenderBarChart from "./GenderBarChart";
import MedalGDPScatter from "./MedalGDPScatter";
import MedalsBubble from "./MedalsBubble";

class Visualization extends React.Component {
  constructor(props) {
    super(props);
    this.state = { tabIndex: 0, customerTabIndex: 0 };
  }

  renderVisualizations = () => {
    const tabTitles = ["city", "company", "job", "Customer's Visualization"];
    const title = tabTitles[this.state.tabIndex];
    if (title == "job") {
      return <JobsBubbleChart />;
    } else if (title == "company") {
      return <CompaniesBarChart />;
    } else if (title == "city") {
      return <CitiesMap />;
    } else if (title == "Customer's Visualization") {
      return (
        <Container>
          <Tabs
            selectedIndex={this.state.customerTabIndex}
            onSelect={(cusTabIndex) =>
              this.setState({ customerTabIndex: cusTabIndex })
            }
          >
            <TabList>
              <Tab>Olympic Champion Gender Visualization</Tab>
              <Tab>GDP and Olympic Medals Visualization</Tab>
              <Tab>Number of Hosted Olympic Games Visualization</Tab>
            </TabList>
          </Tabs>
          {this.renderCustomerVisualizations()}
        </Container>
      );
    }
  };

  renderCustomerVisualizations = () => {
    const tabTitles = ["winnerGender", "MedalGDPScatter", "medalsBubble"];
    const title = tabTitles[this.state.customerTabIndex];
    if (title == "winnerGender") {
      return <GenderBarChart />;
    } else if (title == "MedalGDPScatter") {
      return <MedalGDPScatter />;
    } else {
      return <MedalsBubble />;
    }
  };

  render() {
    return (
      <>
        <Container>
          <br />
          <Tabs
            selectedIndex={this.state.tabIndex}
            onSelect={(tIndex) => this.setState({ tabIndex: tIndex })}
          >
            <TabList>
              <Tab>Cities Visualization</Tab>
              <Tab>Companies Visualization</Tab>
              <Tab>Jobs Visualization</Tab>
              <Tab>Customer's Visualization</Tab>
            </TabList>
          </Tabs>
        </Container>
        <this.renderVisualizations />
      </>
    );
  }
}

export default Visualization;
