import React, { Component } from "react";
import { Container } from "react-bootstrap";
import {
  CartesianGrid,
  Legend,
  Scatter,
  ScatterChart,
  XAxis,
  YAxis,
  Label,
} from "recharts";
import axios from "axios";
import { Redirect } from "react-router-dom";

class MedalGDPScatter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countriesData: [],
    };
  }

  async componentDidMount() {
    axios
      .get("https://api.all-olympian.com/api/countries/")
      .then((response) => {
        console.log(response);
        this.setState({ countriesData: response.data });
      })
      .catch(() => {
        Redirect("/404");
      });
  }

  createPopulationMedalsData() {
    var listData = [];
    var x = this.state.countriesData.length;
    for (var i = 0; i < this.state.countriesData.length; i++) {
      var country = this.state.countriesData[i];
      listData.push({
        name: country["display_name"],
        gdpCapita:
          Number(country["gdp"].replace(/\D/g, "")) /
          100 /
          Number(country["population"]), // divide 100 because took out decimal
        medalCount: country["medal_count"],
      });
    }

    listData.sort(function (a, b) {
      if (a["medalCount"] > b["medalCount"]) {
        return -1;
      } else {
        return 1;
      }
    });
    return listData.slice(0, listData.length - 50);
  }

  render() {
    return (
      <Container>
        <row>
          <h3 className="text-center">
            GDP per Capita vs. Total Olympic Medals Scatter Plot
          </h3>
        </row>
        <row>
          <ScatterChart
            width={800}
            height={700}
            margin={{ top: 20, right: 20, bottom: 20, left: 20 }}
          >
            <Scatter
              name="Countries"
              data={this.createPopulationMedalsData()}
              fill="#8884d8"
              shape="circle"
            />
            <XAxis
              type="number"
              dataKey={"gdpCapita"}
              name="GDP per Capita"
              label={
                <Label
                  value="GDP per Capita"
                  position="bottom"
                  dx={200}
                  style={{ textAnchor: "middle" }}
                />
              }
            />
            <YAxis
              type="number"
              dataKey={"medalCount"}
              name="Number of Medals"
              label={
                <Label
                  angle={-90}
                  value="Total Number of Olympic Medalists"
                  position="left"
                  style={{ textAnchor: "middle" }}
                />
              }
            />
            <CartesianGrid />
            <Legend />
          </ScatterChart>
        </row>
      </Container>
    );
  }
}

export default MedalGDPScatter;
