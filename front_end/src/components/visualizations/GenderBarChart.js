import React, { Component } from "react";
import { Container, Row } from "react-bootstrap";
// import { BarChart } from "react-d3-components";
import {
  Bar,
  BarChart,
  CartesianGrid,
  Legend,
  XAxis,
  YAxis,
  Tooltip,
} from "recharts";
import axios from "axios";
import { Redirect } from "react-router-dom";

class GenderBarChart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      eventsData: [],
    };
  }

  async componentDidMount() {
    axios
      .get("https://api.all-olympian.com/api/events")
      .then((response) => {
        this.setState({ eventsData: response.data });
      })
      .catch(() => {
        Redirect("/404");
      });
  }

  createGenderData() {
    var cleanedData = {}; // use object to easily check for hostingCountry membership
    for (var i = 0; i < this.state.eventsData.length; i++) {
      var event = this.state.eventsData[i];
      var country = event["latest_champion_country"];
      var gender = event["gender"];
      if (country in cleanedData) {
        cleanedData[country][gender] += 1;
      } else {
        cleanedData[country] = {
          name: country,
          male: gender == "male" ? 1 : 0,
          female: gender == "female" ? 1 : 0,
        };
      }
    }

    // console.log(cleanedData);
    var listData = []; // Recharts input is in list form
    for (var country in cleanedData) {
      listData.push(cleanedData[country]);
    }

    // If compareFunction(a, b) returns less than 0,
    // sort a to an index lower than b (i.e. a comes first).
    listData.sort(function (a, b) {
      var aChampions = a["male"] + a["female"];
      var bChamptions = b["male"] + b["female"];
      if (aChampions > bChamptions) {
        return -1;
      } else {
        return 1;
      }
    });
    // console.log(listData);
    return listData.slice(0, 10);
  }

  render() {
    const data = [
      { name: "Page A", mop: 4000, pv: 2400, amt: 2400 },
      { name: "Page B", mop: 3000, pv: 1398, amt: 2210 },
      { name: "Page C", mop: 2000, pv: 9800, amt: 2290 },
      { name: "Page D", mop: 2780, pv: 3908, amt: 2000 },
      { name: "Page E", mop: 1890, pv: 4800, amt: 2181 },
      { name: "Page F", mop: 2390, pv: 3800, amt: 2500 },
      { name: "Page G", mop: 3490, pv: 4300, amt: 2100 },
    ];
    return (
      <Container>
        <row>
          <h3 className="text-center">
            Latest Champion Gender Distribution by Country Bar Chart
          </h3>
        </row>
        <Row>
          <BarChart
            width={1000}
            height={300}
            data={this.createGenderData()}
            margin={{
              top: 20,
              right: 30,
              left: 20,
              bottom: 5,
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            {/* <Tooltip /> */}
            <Legend />
            <Bar
              dataKey="male"
              name="Number of Male Champions"
              stackId="a"
              fill="#8884d8"
            />
            <Bar
              dataKey="female"
              name="Number of Female Champions"
              stackId="a"
              fill="#82ca9d"
            />
          </BarChart>
        </Row>
      </Container>
    );
  }
}

export default GenderBarChart;
