import React, { Component } from "react";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Redirect } from "react-router-dom";

import Splash from "./components/features/Splash";
import About from "./components/features/About";

/* models */
import Cities from "./components/features/CityModel";
import Companies from "./components/features/CompanyModel";
import Jobs from "./components/features/JobModel";

import City from "./components/features/CityDetails";
import Company from "./components/features/CompanyDetails";
import Job from "./components/features/JobDetails";

/* other pages */
import NoMatch from "./components/features/NoMatch";

/* layouts */
import NavigationBar from "./components/features/NavigationBar";
import Footer from "./components/features/Footer";

/* search through all models */
import Search from "./components/features/Search";

import Visualization from "./components/visualizations/visualization";

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <NavigationBar />
        <div class="bg">
          <Router>
            <Switch>
              <Route exact path="/search" component={Search} />
              <Route
                exact
                path="/search/:searchQuerry"
                render={(props) => <Search {...props} />}
              />
              <Route exact path="/" component={Splash} />
              <Route exact path="/about" component={About} />
              <Route exact path="/cities" component={Cities} />
              <Route
                exact
                path="/cities/:cityName"
                render={(props) => <City {...props} />}
              />
              <Route exact path="/companies" component={Companies} />
              <Route
                exact
                path="/companies/:companyName"
                render={(props) => <Company {...props} />}
              />
              <Route exact path="/jobs" component={Jobs} />
              <Route
                exact
                path="/jobs/:jobName"
                render={(props) => <Job {...props} />}
              />
              <Route exact path="/visualizations" component={Visualization} />
              <Route exact path="/404" component={NoMatch} />
              <Redirect to={{ pathname: "/404" }} />
            </Switch>
          </Router>
        </div>
        <Footer></Footer>
      </React.Fragment>
    );
  }
}

export default App;
