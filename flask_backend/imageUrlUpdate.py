import requests
import pprint
from flask import Flask
import json
import sys
import re
import random
from models import Company, City, Role, app, db, generate_picture_url
import pprint
from threading import Thread

# Generates picture based on attribute
def generate_picture_url(attribute):
    url = (
        "https://sweimagesearch.cognitiveservices.azure.com/bing/v7.0/images/search?q="
        + attribute
        + "&count=10&offset=0&mkt=en-us&safeSearch=Moderate"
    )
    headers = {"Ocp-Apim-Subscription-Key": "1c770cf30a8043f182de14b7bbc4bf0a"}
    re = ""

    response = requests.get(url, headers=headers).json()
    try:
        size = len(response["value"])
        if size >= 1:
            size = size - 1
        index = random.randint(0, size)
        re = response["value"][index]["contentUrl"]
    except:
        pass

    return re


# Generates video based on attribute
def generate_videoId(attribute, additionalString=None):
    apikey = "AIzaSyAdFsiJxXkFo-858rSPZ0iGZ7lPtzbvBUg"
    url = (
        "https://www.googleapis.com/youtube/v3/search?part=snippet&key="
        + apikey
        + "&maxResults=2&q="
        + attribute
        + additionalString
    )
    response = requests.get(url).json()
    try:
        videoId = response["items"][0]["id"]["videoId"]
    except KeyError:
        videoId = response["items"][1]["id"]["videoId"]

    return videoId
