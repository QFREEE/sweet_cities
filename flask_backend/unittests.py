from application import application, instance2dict, getUniqueReturn, response, is_number
from models import Company
from unittest import main, TestCase
from companyApi import fixName
from roleApi import get_salary
from cityApi import get_health_care_readable,get_pollution_readable,get_climate_index_readable,get_restaurant_price_index_readable,get_state_from_postal

class UnitTests(TestCase):

    def setUp(self):
        self.instance_company1 = Company(
            id=1,
            name='Sweet Cities',
            company_symbol='SC',
            company_address='1111 MLK Blvd',
            company_city='Austin',
            company_description='Sweet!',
            company_employeeTotal='5',
            company_marketCapitalization=None,
            company_phone='123-456-7890',
            company_weburl=None,
    
        )
        self.instance_company2 = Company(
            id=2,
            name='Sweet Cities2',
            company_symbol='SC2',
            company_address='1111 MLK Blvd2',
            company_city='Austin2',
            company_description='Sweet!2',
            company_employeeTotal='52',
            company_marketCapitalization=None,
            company_phone='123-456-78902',
            company_weburl=None,

        )
        self.instances = []
        self.instances.append(self.instance_company1)
        self.instances.append(self.instance_company2)
    
    
    def test_index(self):
        tester = application.test_client(self)
        response = tester.get('/', content_type='html/text')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'api endpoint for thesweetcities.com', response.data)


    def test_instance2dict(self):
        result = instance2dict(self.instance_company1)
        self.assertIsInstance(result, dict)
        self.assertEqual((result['id']), '1.00')
        self.assertEqual(result['name'], 'Sweet Cities')
        self.assertEqual(result['company_symbol'], 'SC')
        self.assertEqual(result['company_address'], '1111 MLK Blvd')
        self.assertEqual(result['company_city'], 'Austin')
        self.assertEqual(result['company_description'], 'Sweet!')
        self.assertEqual(result['company_employeeTotal'], '5.00')
        self.assertIsNone(result['company_marketCapitalization'])
        self.assertEqual(result['company_phone'], '123-456-7890')
        self.assertIsNone(result['company_weburl'])

    def test_instance2dict2(self):
        result = instance2dict(self.instance_company2)
        self.assertIsInstance(result, dict)
        self.assertEqual((result['id']), '2.00')
        self.assertEqual(result['name'], 'Sweet Cities2')
        self.assertEqual(result['company_symbol'], 'SC2')
        self.assertEqual(result['company_address'], '1111 MLK Blvd2')
        self.assertEqual(result['company_city'], 'Austin2')
        self.assertEqual(result['company_description'], 'Sweet!2')
        self.assertEqual(result['company_employeeTotal'], '52.00')
        self.assertIsNone(result['company_marketCapitalization'])
        self.assertEqual(result['company_phone'], '123-456-78902')
        self.assertIsNone(result['company_weburl'])


    def test_get_unique_return(self):
        result = getUniqueReturn(self.instances)
        self.assertIsInstance(result, list)
        self.assertEqual(result, [1, 2])
    

    def test_response(self):
        d = {'key': 'value', 'number': 1}
        result = response(d)
        stringify = str(result.data, 'utf-8')
        self.assertIn('"key": "value"', stringify)
        self.assertIn('"number": 1', stringify)
        self.assertEqual('200 OK', result.status)
    
    def test_fixName(self):
        name = "car ben"
        res = fixName(name)
        self.assertEqual(res, "Car Ben")

    def test_fixName2(self):
        name = "Car Ben"
        res = fixName(name)
        self.assertEqual(res, name)

    def test_is_number(self):
        num = "123"
        self.assertTrue(is_number(num))

    def test_is_number1(self):
        num = 12
        self.assertTrue(is_number(num))

    def test_is_number2(self):
        num = "ab"
        self.assertFalse(is_number(num))

    def test_is_number2(self):
        num = 1.2
        self.assertTrue(is_number(num))

    def test_get_salary(self):
        test_str = "19283"
        re = get_salary(test_str)
        self.assertEqual("19283",re)

    def test_get_salary2(self):
        test_str = "19,28,3"
        re = get_salary(test_str)
        self.assertEqual("1928",re)

    def test_get_salary3(self):
        test_str = "19283A"
        re = get_salary(test_str)
        self.assertEqual("19283",re)

    def test_get_salary4(self):
        test_str = "19283AA"
        re = get_salary(test_str)
        self.assertEqual("19283",re)

    def test_get_health_care_readable(self):
        test = 20
        re = get_health_care_readable(test)
        self.assertEqual("LOW",re)

    def test_get_health_care_readable1(self):
        test = 100
        re = get_health_care_readable(test)
        self.assertEqual("HIGH",re)

    def test_get_pollution_readable(self):
        test = 40
        re = get_pollution_readable(test)
        self.assertEqual("MEDIUM",re)

    def test_get_pollution_readable2(self):
        test = 100
        re = get_pollution_readable(test)
        self.assertEqual("HIGH",re)

    def test_get_climate_index_readable(self):
        test = 40
        re = get_climate_index_readable(test)
        self.assertEqual("LOW",re)

    def test_get_climate_index_readable2(self):
        test = 10
        re = get_climate_index_readable(test)
        self.assertEqual("LOW",re)

    def test_get_restaurant_price_index_readable(self):
        test = 10
        re = get_restaurant_price_index_readable(test)
        self.assertEqual("LOW",re)

    def test_get_restaurant_price_index_readable(self):
        test = 80
        re = get_restaurant_price_index_readable(test)
        self.assertEqual("HIGH",re)

    def test_get_state_from_postal(self):
        test = "AL"
        re = get_state_from_postal(test)
        self.assertEqual("Alabama",re)

    def test_get_state_from_postal2(self):
        with self.assertRaises(KeyError):
            test = "XY"
            re = get_state_from_postal(test)
        


if __name__ == "__main__":
    main()
