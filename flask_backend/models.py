import requests
import pprint
from flask_sqlalchemy import SQLAlchemy
from flask import Flask
import json
import sys
from sqlalchemy import create_engine
import random


# Connects to the database
application = app = Flask(__name__)
application.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
application.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql+psycopg2://sweetcityAdmin:Sw33tC1ty@sweet-cities-db-1.cs1ogtytf7sx.us-east-1.rds.amazonaws.com:5432/postgres"
db = SQLAlchemy(application)

# Represents the Company Model in the database
class Company(db.Model):
    __tablename__ = "Company"
    __table_args__ = {"schema": "public"}
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.Text, nullable=False)
    company_symbol = db.Column(db.Text, nullable=False)
    company_address = db.Column(db.Text, nullable=True, unique=False)
    company_city = db.Column(db.Text, nullable=True, unique=False)
    company_state = db.Column(db.Text, nullable=True, unique=False)
    company_description = db.Column(db.Text, nullable=True, unique=False)
    company_employeeTotal = db.Column(db.Float(), nullable=True, unique=False)
    company_employeeTotal_readable = db.Column(db.Text(), nullable=True, unique=False)

    company_marketCapitalization = db.Column(db.Float(), nullable=True, unique=False)
    company_marketCapitalization_readable = db.Column(
        db.Text(), nullable=True, unique=False
    )

    company_phone = db.Column(db.Text, nullable=True, unique=False)
    company_weburl = db.Column(db.Text, nullable=True)
    company_price = db.Column(db.Float(), unique=False)
    company_price_readable = db.Column(db.Text(), nullable=True, unique=False)

    picture_url = db.Column(db.Text, unique=False)
    video_url = db.Column(db.Text, nullable=True, unique=False)

    def __repr__(self):
        return f"Company( '{self.name}','{self.company_state}')"

    def as_dict(self):
        return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}


# Represents the City Model in the database
class City(db.Model):
    __tablename__ = "City"
    __table_args__ = {"schema": "public", "extend_existing": True}
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.Text(), nullable=False, unique=False)
    city_postal_state = db.Column(db.Text)
    city_state = db.Column(db.Text)
    city_weather = db.Column(db.Text(), nullable=True, unique=False)

    health_care_index = db.Column(db.Float(), nullable=True, unique=False)
    health_care_readable = db.Column(db.Text(), nullable=True, unique=False)

    pollution_index = db.Column(db.Float(), nullable=True, unique=False)
    pollution_readable = db.Column(db.Text(), nullable=True, unique=False)

    climate_index = db.Column(db.Float(), nullable=True, unique=False)
    climate_index_readable = db.Column(db.Text, nullable=False)

    restaurant_price_index = db.Column(db.Float(), nullable=True, unique=False)
    restaurant_price_index_readable = db.Column(db.Text, nullable=False)

    picture_url = db.Column(db.Text, unique=False)
    video_url = db.Column(db.Text, nullable=True, unique=False)

    def __repr__(self):
        return f"City('{self.name}', '{self.city_state}')"

    def as_dict(self):
        return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}


# Represents the Role Model in the database
class Role(db.Model):
    __tablename__ = "Role"
    __table_args__ = {"schema": "public"}
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.Text())
    role_description = db.Column(db.Text())
    low_salary = db.Column(db.Integer)
    median_salary = db.Column(db.Integer)
    high_salary = db.Column(db.Integer)
    daily_responsibility = db.Column(db.Text())
    alternative_titles = db.Column(db.Text())
    top_cities = db.Column(db.Text())
    top_companies = db.Column(db.Text())
    skills = db.Column(db.Text())
    trainings = db.Column(db.Text())
    role_state = db.Column(db.Text())
    picture_url = db.Column(db.Text, unique=False)
    video_url = db.Column(db.Text, nullable=True, unique=False)

    def __repr__(self):
        return f"Role('{self.name}','{self.role_state})"

    def as_dict(self):
        return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}


# Generates picture based on attribute
def generate_picture_url(attribute):
    url = "https://api.unsplash.com/search/photos?page=1&query="
    sercet = "&client_id=AwYlLV8NBuphVLuhMWZU8LyfczY6h2jnNJtdKl5_usI"
    re = ""
    try:
        response = requests.get(url + attribute + sercet).json()
        size = len(response["results"])
        if size >= 1:
            size = size - 1
        index = random.randint(0, size)
        re = response["results"][index]["urls"]["small"]
    except:
        pass
    return re


# Generates picture based on attribute
def generate_video_url(attribute, additionalString=None):
    # apikey = "AIzaSyAdFsiJxXkFo-858rSPZ0iGZ7lPtzbvBUg"
    # apikey = "AIzaSyCMwiiz86_LJdqJFhiQJWBhCJVAnmWAm3E"
    # apikey = "AIzaSyB_6925CKw9NnZ6o08tNQ9CZiocVxV9TI4"
    apikey = "AIzaSyClq_qHlhP9BXhl2YnNqUgUQZBYvQ9SX9w"

    url = (
        "https://www.googleapis.com/youtube/v3/search?part=snippet&key="
        + apikey
        + "&maxResults=2&q="
        + attribute
        + additionalString
    )
    response = requests.get(url).json()
    print(response)
    try:
        videoId = response["items"][0]["id"]["videoId"]
    except (KeyError, IndexError) as error:
        try:
            videoId = response["items"][1]["id"]["videoId"]
        except (KeyError, IndexError) as error2:
            videoId = "GY8PkikQ8ZE"

    return "https://www.youtube.com/embed/" + videoId


# Creates engine for database
engine = create_engine(application.config["SQLALCHEMY_DATABASE_URI"])
