MAKEFLAGS += --no-builtin-rules
# backend:
# 	cd ./backend
# 	python3 manage.py runserver 

# frontend:
# 	cd ./front_end
# 	nmp start

venv:
	virtualenv venv

front_server:
	cd ./front_end; yarn start;

# install node_modules in front_ends
node_modules:
	cd ./front_end; npm init;

clean:
	rm -f  .coverage
	rm -f  .pylintrc
	rm -f  *.pyc
	rm -f  *.tmp
	rm -rf __pycache__
	rm -rf .mypy_cache
	rm -f backend.zip

build_db:
	cd ./flask_backend
	python3 companyApi.py
	python3 roleApi.py
	python3 cityApi.py

backend:
	zip backend.zip ./flask_backend/modles.py ./flask_backend/application.py ./flask_backend/requirments.txt
